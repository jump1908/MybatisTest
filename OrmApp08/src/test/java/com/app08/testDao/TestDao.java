package com.app08.testDao;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app08.dao.UserMapper;
import com.app08.domain.User;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
//配置了@ContextConfiguration注解并使用该注解的locations属性指明spring和配置文件之后，
@ContextConfiguration(locations = {"classpath:config/spring.xml", "classpath:config/spring-mybatis.xml" })
public class TestDao {

	@Autowired
	UserMapper userMapper;
	
	@Test
	public void addUser() {
		User user = new User();
		user.setUserName("罗辑");
		user.setUserBirthday(new Date());
		user.setUserSalary(9000D);
		userMapper.addUser(user);
	}
	
	@Test
	public void getUser() {
		User user = userMapper.getUser(3);
		System.out.println(user.getUserName());
	}
	
}
