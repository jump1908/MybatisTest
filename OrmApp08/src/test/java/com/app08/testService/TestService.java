package com.app08.testService;

import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.app08.domain.User;
import com.app08.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
//配置了@ContextConfiguration注解并使用该注解的locations属性指明spring和配置文件之后，
@ContextConfiguration(locations = {"classpath:config/spring.xml", "classpath:config/spring-mybatis.xml" })
public class TestService{

    //注入userService
    @Autowired
    private UserService userService;
    
    @Test
    public void testAddUser(){
        User user = new User();
        user.setUserName("章北海");
        user.setUserBirthday(new Date());
        user.setUserSalary(10000D);
        userService.addUser(user);
    }
    
    @Test
    public void testUpdateUser() {
    	User user = new User();
    	user.setUserId(2);
    	user.setUserName("杨冬");
    	user.setUserSalary(8000D);
    	userService.updateUser(user);
    }
    
    @Test
    public void testGetUser(){
        Integer userId = 1;
        User user = userService.getUser(userId);
        System.out.println(user.getUserName());
    }
}
