package com.app08.dao;

import java.util.List;


import com.app08.domain.User;

public interface UserMapper {

	public User getUser(Integer userId);
	public int addUser(User user);
	public int deleteUser(String userName);
	public int updateUser(User user);
	public List<User> getAllUser();
}
