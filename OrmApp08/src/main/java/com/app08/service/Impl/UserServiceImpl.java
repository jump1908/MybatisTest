package com.app08.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app08.dao.UserMapper;
import com.app08.domain.User;
import com.app08.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	
    /**
     * 使用@Autowired注解标注userMapper变量，
   
  * 当需要使用UserMapper时，Spring就会自动注入UserMapper
     */
    @Autowired
    private UserMapper userMapper;//注入dao
    
	@Override
	public User getUser(Integer userId) {
		// TODO Auto-generated method stub
		User user = userMapper.getUser(userId);
		return user;
	}

	@Override
	public int addUser(User user) {
		// TODO Auto-generated method stub
		int i = userMapper.addUser(user);
		return i;
	}

	@Override
	public int deleteUser(String userName) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUser(User user) {
		// TODO Auto-generated method stub
		userMapper.updateUser(user);
		return 0;
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return null;
	}

}
