package com.app08.service;

import java.util.List;

import com.app08.domain.User;

public interface UserService {

	public User getUser(Integer userId);
	public int addUser(User user);
	public int deleteUser(String userName);
	public int updateUser(User user);
	public List<User> getAllUser();
	
}
