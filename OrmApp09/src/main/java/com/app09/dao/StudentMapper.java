package com.app09.dao;

import com.app09.bean.Student;
import java.util.List;

public interface StudentMapper {
    int deleteByPrimaryKey(Integer sId);

    int insert(Student record);

    Student selectByPrimaryKey(Integer sId);

    List<Student> selectAll();

    int updateByPrimaryKey(Student record);
}