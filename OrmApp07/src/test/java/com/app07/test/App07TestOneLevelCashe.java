package com.app07.test;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.app07.Utils.MybatisUtil;
import com.app07.domain.User;

/**
 * Unit test for simple App.
 */
public class App07TestOneLevelCashe {
	
	@Test
	public void testCashe1() {
		SqlSession session = MybatisUtil.getSqlSession();
		String statement = "com.app07.mapping.UserMapper.getUser";
		User user = session.selectOne(statement, 1);
		System.out.println(user);
		
		/*
		 * 一级缓存默认就会被使用
		 */
		user = session.selectOne(statement, 1);
		System.out.println(user);
		session.close();
		
		/*
		 * 1.必须是同一个Session，如果session对象已经close()过了就不可能用了
		 */
		session = MybatisUtil.getSqlSession();
		user = session.selectOne(statement, 1);
		System.out.println(user);
		
		/*
		 * 2.查询条件是一样的
		 */
		user = session.selectOne(statement, 2);
		System.out.println(user);
		
		/*
		 * 3.没有执行过session.clearCache()清理缓存
		 */
		//session.clearCache();
		user = session.selectOne(statement, 2);
		System.out.println(user);
		
		/*
		 * 4.没有执行过增删改查的操作（这些操作都会清理缓存）
		 */
		session.update("com.app07.mapping.UserMapper.updateUser", new User(2, "杨冬", 23));
		user = session.selectOne(statement, 2);
		System.out.println(user);
	}
	
}
