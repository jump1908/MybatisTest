package com.app02.OrmApp02;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.junit.*;

import com.app02.Utils.MybatisUtil;
import com.app02.domain.User;

public class App02Test {

	@Test
	public void test() {
		/*
		 * 根据MybatisUtil工具类获取SqlSession
		 */
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);
        /*
         * 映射sql的标识字符串
         * com.app02.mapping.UserMapper是UserMapper.xml文件中的mapper标签的namespace属性的值，
         * addUser是insert表情的id属性值，通过insert标签的id属性值就可以找到要执行的SQL
         */
        String statement = "com.app02.mapping.UserMapper.getUser";//映射sql的标识字符串
        //执行查询返回一个唯一user对象的sql
        User user = sqlSession.selectOne(statement, 1);
        System.out.println(user.getName());
        sqlSession.close();
    }
	
	@Test
	public void testAdd() {
		/*
		 * 根据MybatisUtil工具类获取SqlSession
		 */
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);
        /*
         * 映射sql的标识字符串
         * com.app02.mapping.UserMapper是UserMapper.xml文件中的mapper标签的namespace属性的值，
         * addUser是insert表情的id属性值，通过insert标签的id属性值就可以找到要执行的SQL
         */
        String statement = "com.app02.mapping.UserMapper.addUser";
        User user = new User();
        user.setName("罗辑");
        user.setAge(25);
        //执行插入操作
        int retResult = sqlSession.insert(statement, user);
        /*
         * 手动提交事务
         * sqlSession.commit()
         * 使用SqlSession执行完SQL之后需要关闭SqlSession
         */
        sqlSession.close();
        System.out.println(retResult);
    }
	
	@Test
	public void testUpdate() {
		/*
		 * 根据MybatisUtil工具类获取SqlSession
		 */
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);
        /*
         * 映射sql的标识字符串
         * com.app02.mapping.UserMapper是UserMapper.xml文件中的mapper标签的namespace属性的值，
         * addUser是insert表情的id属性值，通过insert标签的id属性值就可以找到要执行的SQL
         */
        String statement = "com.app02.mapping.UserMapper.updateUser";
        User user = new User();
        user.setId(3);
        user.setName("庄颜");
        user.setAge(19);
        //执行插入操作
        int retResult = sqlSession.update(statement, user);
        /*
         * 手动提交事务
         * sqlSession.commit()
         * 使用SqlSession执行完SQL之后需要关闭SqlSession
         */
        sqlSession.close();
        System.out.println(retResult);
	}
	
	@Test
	public void testDelete() {
		/*
		 * 根据MybatisUtil工具类获取SqlSession
		 */
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);
        /*
         * 映射sql的标识字符串
         * com.app02.mapping.UserMapper是UserMapper.xml文件中的mapper标签的namespace属性的值，
         * addUser是insert表情的id属性值，通过insert标签的id属性值就可以找到要执行的SQL
         */
        String statement = "com.app02.mapping.UserMapper.deleteUser";
        //执行删除操作
        int retResult = sqlSession.delete(statement, "罗辑"); //删除叫罗辑的用户
        /*
         * 手动提交事务
         * sqlSession.commit()
         * 使用SqlSession执行完SQL之后需要关闭SqlSession
         */
        sqlSession.close();
        System.out.println(retResult);
	}
	
	@Test
	public void testGetAll() {
		/*
		 * 根据MybatisUtil工具类获取SqlSession
		 */
        SqlSession sqlSession = MybatisUtil.getSqlSession(true);
        /*
         * 映射sql的标识字符串
         * com.app02.mapping.UserMapper是UserMapper.xml文件中的mapper标签的namespace属性的值，
         * addUser是insert表情的id属性值，通过insert标签的id属性值就可以找到要执行的SQL
         */
        String statement = "com.app02.mapping.UserMapper.getAllUsers";
        //执行删除操作
        List<User> userList = sqlSession.selectList(statement); //删除叫罗辑的用户
        /*
         * 手动提交事务
         * sqlSession.commit()
         * 使用SqlSession执行完SQL之后需要关闭SqlSession
         */
        sqlSession.close();
        System.out.println(userList.size());
	}
}
