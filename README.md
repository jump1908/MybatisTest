# MybatisTest

#### 项目介绍
用于学习Mybatis持久化框架

#### 主要内容如下
1. Mybatis入门
2. Mybatis的CRUD操作
3. Mybatis配置文件优化
4. Mybatis解决字段名与实体类属性名不同
5. Mybatis实现多表联查
6. Mybatis调用存储过程
7. Mybatis缓存入门
8. Mybatis与Spring集成以及测试